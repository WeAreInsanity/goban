//###############################################################################
//Goban
//This program is used for playing Go. Players would be able to use this program
//as a virtual go board (or goban) to play a game of go.
//###############################################################################

#include <iostream>

using namespace std;

//Class for each point on the board
class Point {
public:
    int x, y;                   //The x, y coordinates of the point
    string icon = "+ ";         //The icon which will be printed
    bool hasStone;              //Is the point occupied
    bool stoneColor;            //What color is the stone (default: black)
    Point ();
    void setPoint (int, int);
    void setIcon (int, int);
    string returnPoint ();
};

//This function sets the icon for the object for when the printGoban function
//prints the board. + represents an empty point, o represents a star point,
//O represents a black stone, and X represents a white stone.
void Point::setIcon(int x, int y) {
    //Start by setting the icon to be a stone if applicable
    if (hasStone) {
        if (!stoneColor) {
            icon = "O ";        //Set to black
        } else {
            icon = "X ";        //Set to white
        }
    }
    //Otherwise, check to see if the stone is a star point
    else {
        if ((x == 3) && ((y == 3) || (y == 9) || (y == 15))) {
            icon = "o ";
        } else if ((x == 9) && ((y == 3) || (y == 9) || (y == 15))) {
            icon = "o ";
        } else if ((x == 15) && ((y == 3) || (y == 9) || (y == 15))) {
            icon = "o ";
        }
    }
}

//Constructor for point, sets default coordinate to 0,0
//then sets the icon for the point.
Point::Point() {
    x = 0;
    y = 0;
    setIcon(x, y);
}

//Set the coordinates of the point
void Point::setPoint (int xPoint, int yPoint){
    x = xPoint;
    y = yPoint;
}

//Return the coordinate of the point as a string
string Point::returnPoint (){
    int thisPoint[] = {x, y};
    return to_string(thisPoint[0]) + "," + to_string(thisPoint[1]);
}

//Create matrix for points
Point pointSet[19][19];

//Turn class, holds information for each play and functions for each as well
class Turn {
public:
    //Player chosen X, Y coordinates
    int turnX, turnY;       //The coordinate of the current move
    bool recentPass;        //Indicates if the last turn was a pass
    bool thisTurn;          //Indicates whose turn it is
    int turnCount;          //Indicates how many turns have taken place
    int askMove();
    int setPoint(Point&);

};

//Ask the player what move they'd like to do.
//This function also determins if the game is over or not.
//Returns 0 if the game continues, 1 if the game is over.
int Turn::askMove() {
    //Determine which player is to move
    string player = "";
    if ((turnCount % 2) == 0) {
        player = "Black";

    } else {
        player = "White";
    }

    //Prompt player to move
    cout << player << " to move. Enter 50 for pass, 99 for resign, or X Y positions of stone: " << endl;
    cin >> turnX >> turnY;

    //Determine if game continues
    if (turnX == 99) {                          //Return 1 if resign
        cout << player << " has resigned." << endl;
        return 1;
    } else if (turnX == 50) {                   //Return 0 if pass, but return 1 if
                                                //both players have passed in a row
        cout << player << " passes." << endl;
        if (recentPass == 0) {
            recentPass = 1;
            return 0;
        }
        else {
            return 1;
        }
    } else {                                   //Return 0 if move successful
        cout << player << " chose " << turnX << ", " << turnY << endl;
        return 0;
    }

}

//Place the stone on the board, determines if move is legal or not
//Returns 0 for legal moves, returns 1 for illegal moves
int Turn::setPoint(Point& thisPoint) {
    if (thisPoint.hasStone) {                   //Make sure no stone in place
        cout << "Invalid placement: That spot already has a stone!";
        return 1;
    } else if (turnX == 50){                    //Contingency for pass
        turnCount++;
        return 0;
    } else if ((turnX > 18) || (turnY > 18)){   //Contingencies for when selected
                                                //placement is outisdeof board bounds.
        cout << "Invalid placement: that spot does not exist!";
        return 1;
    } else if ((turnX < 0) || (turnY < 0)){
        cout << "Invalid placement: that spot does not exist!";
        return 1;
    } else {                                    //If all checks out, place the stone
        thisPoint.hasStone = 1;
        thisPoint.stoneColor = thisTurn;
        thisPoint.setIcon(thisPoint.x, thisPoint.y);
        turnCount++;
        return 0;
    }
}

//This fucntion prints the current board.
void printGoban(){
    for (int i=0; i<19; i++){               //The vertical location
        for (int j=0; j<19; j++){           //The horizontal location
            cout << pointSet[i][j].icon;    //Print the point icon of the current X Y location
            if (j == 18) {
                cout << endl;               //If we're at the end of row, end line
            }
        }
    }
}

//This function sets the points of all the objects in pointSet
//to their respective values. For example, for pointSet[3][2],
//its x value is set to 3 and its y value is set to 2.
void generateBoard() {
    //Set all coordinate points for the board
    for (int i = 0; i < 19; i++){
        for (int j = 0; j <19; j++) {
            //Set points
            pointSet[i][j].setPoint(i, j);
        }
    }

}


int main() {

    Turn turn;
    generateBoard();
    printGoban();
    int gameStatus = 0; //Determines if game is continuous or not.
                        //0 means game is active, 1 means game is over.

    //Loops turn functions. Ends loop when gameStatus changes to 1.
    do {
        gameStatus = turn.askMove();
        turn.setPoint(pointSet[turn.turnX][turn.turnY]);
        printGoban();
    } while (gameStatus == 0);

    return 0;
}